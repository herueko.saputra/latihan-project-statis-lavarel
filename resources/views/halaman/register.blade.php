@extends('layout.master')

@section('judul')
Buat Account Baru
@endsection
    
@section('content')
    <h1>Buat Account Baru</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="post">
        @csrf
        <label>First name</label><br><br>
        <input type="text" name="firstname"><br><br>
        <label>Last name</label><br><br>
        <input type="text" name="lastname"><br><br>
            
    <p>Gender:</p>
        <input type="radio" id="male" value="male">
        <label for="male">Male</label><br>
        <input type="radio" id="female" value="female">
        <label for="female">Female</label><br>
   
    <p>Nationality:</p>
        <select id="nation" name="nation">
        <option value="indo">Indonesia</option>
        <option value="malay">Malaysia</option>
        <option value="singa">Singapura</option>
        </select>
    
    <p>Language Spoken</p>
        <input type="checkbox" id="lang1" value="Bahasa Indonesia">
        <label for="lang1">Bahasa Indonesia</label><br>
        <input type="checkbox" id="lang2" value="English">
        <label for="lang2">English</label><br>
        <input type="checkbox" id="lang3" value="Other">
        <label for="lang3">Other</label><br>
    
    <p>Bio:</p>
    <textarea cols="30" rows="10"></textarea> <br><br>
    <input type="submit" value="sign up">
    </form>
    
@endsection